<script src="/js/user_area/create_pacient.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.14.0/jquery.validate.min.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.14.0/localization/messages_ro.js"></script>
<style>
<!--
.error{
    color: red
}
-->
</style>
	<div class="row">
		<div class="col-md-10 col-md-offset-1" id="pacient_details">
			<div class="panel panel-default">
				<div class="panel-heading">Adauga Pacient</div>
				<div class="panel-body">

						<form action="/pacient/submit_questionnaire" id="pacient_details_form">
						<div class="form-group">
							<label class="col-md-4 control-label">Nume</label>
							<div class="col-md-6">
								<input type="text" class="form-control input-sm" name="nume" id="nume" placeholder="Nume"
									value="{{ old('nume') }}" required> 
							</div>
							<label class="col-md-4 control-label">Prenume</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="prenume" placeholder="Prenume"
									value="{{ old('prenume') }}" required> 
							</div>
							<label class="col-md-4 control-label">CNP</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="cnp" placeholder="CNP"
									value="{{ old('cnp') }}" required> 
								
							</div>
							<label class="col-md-4 control-label">Nume Mama</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="mama"  placeholder="Nume Mama"
									value="{{ old('mama') }}">
							</div>
							<label class="col-md-4 control-label">Nume Tata</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="tata" placeholder="Nume Tata"
									value="{{ old('tata') }}">
							</div>
							<label class="col-md-4 control-label">Adresa</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="adresa" placeholder="Adresa"
									value="{{ old('adresa') }}" required>
							</div>
							
							<label class="col-md-4 control-label">Judet</label>
							<div class="col-md-6">
								
								<select id="judet" name="judet" required class="form-control" placeholder="Judet" required>
								<option value="">Selecteaza judet</option>
								@foreach ($counties as $county)
								    <option value="{{ $county->id }}">{{ $county->name }}</option>
								@endforeach
								</select>
							</div>
							<label class="col-md-4 control-label">Localitate</label>
							<div class="col-md-6">
								<select id="localitate" name="localitate" required class="form-control" required>
								<option value="">Selecteaza judet intai</option>
								
								</select>
							</div>
							<label class="col-md-4 control-label">Email</label>
							<div class="col-md-6">
								<input type="email" class="form-control" name="email" placeholder="Email" required
									value="{{ old('email') }}"> 
							</div>
							<label class="col-md-4 control-label">Telefon</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="telefon" placeholder="Telefon"
									value="{{ old('telefon') }}" required> 
							</div>
							<label class="col-md-4 control-label">Observatii Medicale</label>
							<div class="col-md-6">
								<textarea id="observatii_medicale" class="form-control" name="observatii_medicale">{{ old('observatii_medicale') }}</textarea>
							</div>
							<label class="col-md-4 control-label"> </label>
							<div class="col-md-10">
								<button class="btn btn-primary btn-block buton_next" data-pas="0" type="button">Continua</button>
							</div>
						</div>
					</form>
			</div>
		</div>
	</div>
