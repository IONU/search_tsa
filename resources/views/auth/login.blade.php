@extends('layouts.app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading"><h3>despre SEARCH TSA</h3></div>
				
				<div class="panel-body">
					<ul>Proiectul <strong>SEARCH TSA</strong>  vizeaza trei arii importante:<br/><br/>

						<li>crearea unei baze de date centralizate si platforma software necesara pentru <strong>screening-ul initial al copiilor pentru tulburari din spectrul autist</strong>; 

						<li><strong>crearea unei platforme cu teste de evaluare standardizate</strong> pentru evaluarea copiilor cu autism de catre psihologi;</li>

						<li><strong>infiintarea  unui HELPLINE AUTISM</strong> pentru a veni in sprijinul familiilor care se confrunta cu aceasta problema, dar si a	celorlator categorii de actori sociali care vin in contact cu copiii;</li>

						<li><strong>crearea de  50 de aplicatii pentru tableta si telefon mobil</strong> care ofera o  interfata foarte utila si motivanta in interventiile terapeutice si educationale
							destinate copiilor cu autism. Tehnologia  poate fi folosita pentru a imbunatati comunicarea verbala, dar in acelasi timp ofera o alternativa de comunicare prin aplicatii pentru copiii non-verbali; ajuta la dezvoltarea aptitudinilor sociale prin jocurile specifice
							disponibile; sporesc capacitatea de a invata prin stimulii vizuali, auditivi  si tactili. Aplicatiile existente in acest moment sunt fie in limba engleza, fie au minusuri cu privire la continut acestea fiind create pentru copii tipici, nu pentru copii cu autism.</li>
					</ul>
				</div>
				<div class="panel-heading"><h3>platforma de screening a autismului - SearchTSA</h3></div>
				<div class="panel-body center-txt">
					<iframe width="560" height="315" src="https://www.youtube.com/embed/y3zYXr8GNtk" frameborder="0" allowfullscreen></iframe>
					
				</div>
				<div class="panel-body">
					Am dezvoltat un test pentru identificarea precoce a suspiciunii legate de autism. Testul poate fi folosit astfel: 
					<ul>
						<li><h4>de catre medicii de familie</h4> in cadrul Programului National de Screening al Autismului - este necesara autentificarea in cadrul site-ului SearchTSA.ro</li>
						<li><h4>de catre parinti/educatori</h4> in cazul in care au suspiciuni legate de autism pentru copilul lor - nu este necesara autentificarea, chestionarul poate fi completat sub protectia anonimatului</li>
					</ul>
					
					
				</div>
				<div class="panel-heading"><h3>Helpline Autism - 0800 500 AUT (288)</h3></div>
				<div class="panel-body center-txt">
					<iframe width="560" height="315" src="https://www.youtube.com/embed/2ZKkpyQRc2s" frameborder="0" allowfullscreen></iframe>
				</div>
				<div class="panel-body">
					Esti parintele unui copil despre care crezi ca ar putea avea o afectiune de tulburare din spectrul autist sau esti educatorul unui astfel de copil? Suna gratuit la <strong>Infoline Autism 0800 500 AUT(288)</strong> si consilierii nostri sunt pregatiti sa te ajute!
				</div>
				<div class="panel-heading">Login | medici de familie</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<form class="form-horizontal" role="form" method="POST" action="/auth/login">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group">
							<label class="col-md-4 control-label">Adresa Email</label>
							<div class="col-md-6">
								<input type="email" class="form-control" name="email" value="{{ old('email') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Parola</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary" style="margin-right: 15px;">
									Login
								</button>

								<a href="/password/email">Ai uitat parola?</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
