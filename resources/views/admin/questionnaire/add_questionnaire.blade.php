@extends('layouts.app')

@section('content')

    <style>
        #form label{
            display: inline-block;
            float: left;
            clear: left;
            width: 150px;
            text-align: right;
            margin-right: 10px;
            margin-top: 10px;
        }
        #form select, textarea {
            margin-top: 10px;
            width: 450px;
        }

        #form input {
            margin-top: 10px;
            width: 50px;
        }

        #form p {
            display: inline-block;
            float: left;
            margin-top: 10px;
            color: red;
        }

        #form input[type="submit"]{
            margin-top: 18px;
            width: 450px;
        }
        #form input[type="button"]{
            margin-top: 18px;
            width: 450px;
        }
        .selectize-input {
            width: 450px !important;
        }

        span.raspuns {
            width: 400px;
            float:left;
            margin-left:10px;
            margin-bottom: 10px;
        }
        
        div.intrebare {
            float: left;
        }

        .criteriu {
            font-style: italic;
            color : darkblue;
            margin-left: 20px;;
        }

    </style>


    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Creaza un chestionar</div>
                    <div class="panel-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li> @endforeach
                                </ul>
                            </div>
                        @endif

                        <form id="form" method="post" action="{{ url() }}/do_add_questionnaire">

                            <div class="form-group">
                                <label class="control-label">Tip Chestionar : </label>
                                <select name="questionnaire_type" id ="questionnaire_type">
                                    <option value=""></option>
                                    @foreach($questionnarie_types as $questionnarie_type)
                                        <option value={{$questionnarie_type->id}}> {{$questionnarie_type->questionnaire_type_name}}</option>
                                    @endforeach
                                </select></br>
                                <label class="control-label">Categorie Chestionar : </label>
                                <select name="questionnaire_category" id ="questionnaire_category">
                                        <option value=""></option>
                                    @foreach($questionnarie_categories as $questionnarie_category)
                                        <option value={{$questionnarie_category->id}}> {{$questionnarie_category->category_name}}</option>
                                    @endforeach
                                </select></br>
                                <label class="control-label">Subcategorie Chestionar : </label>
                                <select name="questionnaire_subcategory" id ="questionnaire_subcategory">
                                    <option value=""></option>
                                </select></br>
                                <label class="control-label">Nr crt.</label>
                                <input type="text" id="question[1][ordine]" name="question[1][ordine]" /></br>
                                <label class="control-label">Intrebare : </label>
                                <textarea  id="question[1][intrebare]" name="question[1][intrebare]" cols="40%"></textarea></br>
                                <label class = 'control-label'>Atribuie un crieteriu de evaluare</label>
                                <select name="question[1][evaluation_criteria_id]" class="criteria">
                                    <option value=""></option>
                                </select>
                                <div class = 'add_area'></div>
                                <label> </label><input class="btn btn-info btn-lg " type="button" id='row' value="Adauga intrebari in categorie"/>
                            </div>

                            <label> </label><input class="btn btn-success btn-lg btn-block" type="submit" id="submit" value="Submit"/>
                        </form>
                    </div>
                    <div class="panel-heading">Lista intrebari</div>
                    <div class="panel-body">
                        <div>
                            <div class="lista_intrebari" >
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--<script src="/js/user_area/create_pacient.js"></script>--}}
    {{--<script src="/js/admin_area/QuestionsAddCtrl.js"></script>--}}

    <script type="text/javascript">
        $(document).ready(function () {
            var max_fields = 100; //maximum input boxes allowed
            var wrapper = $(".add_area"); //Fields wrapper
            var add_conditions = $("#row"); //Add button ID


            var x = 1; //initlal text box count

            $(add_conditions).on('click', function (e) { //on add input button click

                var type_id = $('#questionnaire_type').val();
                var category_id = $('#questionnaire_category').val();
                var subcategory_id = $('#questionnaire_subcategory').val();

                e.preventDefault();
                if (x < max_fields) { //max input box allowed
                    x++; //text box increment
                    $(wrapper).append('<div><label class="control-label">Nr. Crt.</label>' +
                    '<input type="text" id="question[' + x + '][ordine]" name="question[' + x + '][ordine]" /></br>' +
                    '<label class="control-label">Intrebare : </label>' +
                    '<textarea name="question[' + x + '][intrebare]"  name="question['+ x + '][intrebare]" cols="40%"></textarea>' +
                    '<label class = "control-label">Atribuie un crieteriu de evaluare</label>' +
                    '<select name="question[' + x + '][evaluation_criteria_id]"  id="criteria_id_' + x + '" class="criteria"><option value=""></option> </select>' +
                    '</span>&nbsp;<a href="#" class="remove_field">Remove</a><div>');


                    var _selectize_criteria = $('#criteria_id_' + x).selectize({
                        plugins: ['restore_on_backspace'],
                        delimiter: ',',
                        placeholder: 'Selectati criteriul de evaluare pentru intrebare ',

                        create:false
                    });

                    get_evaluation_criteria(type_id, category_id, subcategory_id, _selectize_criteria);
                }
            });


            $(wrapper).on("click", ".remove_field", function (e) { //user click on remove text
                e.preventDefault();
                $(this).parent('div').remove();
                x--;

            });
            $('select#questionnaire_category').selectize({
                plugins: ['restore_on_backspace'],
                delimiter: ',',
                placeholder: 'Selectati o optiune. Daca aceasta nu exista, o puteti introduce',

                create: function (option) {
                    var id;
                    $.ajax({
                        async: false,
                        url: "/do_add_questionnaire_category",
                        data: {category_name: option},
                        type: 'POST',
                        success: function (data) {
                            id = jQuery.parseJSON(data);
                            return id;
                        }
                    });
                    $('#questionnaire_category').append(
                            $('<option></option>').val(id).html(option)
                    );
                    return {
                        value: id,
                        text: option
                    }
                }
            });

            $('select#questionnaire_type').selectize({
                plugins: ['restore_on_backspace'],
                delimiter: ',',
                placeholder: 'Selectati o optiune. Daca aceasta nu exista, o puteti introduce',

                create: function (option) {
                    var id;
                    $.ajax({
                        async: false,
                        url: "/do_add_questionnaire_type",
                        data: {questionnaire_type_name: option},
                        type: 'POST',
                        success: function (data) {
                            id = jQuery.parseJSON(data);
                            return id;
                        }
                    });
                    $('#questionnaire_type').append(
                            $('<option></option>').val(id).html(option)
                    );
                    return {
                        value: id,
                        text: option
                    }
                }
            });


            $('#questionnaire_type,#questionnaire_category').change(function() {
                $.ajax({
                    url: "/get_subcategories",
                    data: {category_id: $('#questionnaire_category').val(), type_id: $('#questionnaire_type').val()},
                    type: 'GET',
                    success: function (data) {
                        $('#questionnaire_subcategory').empty().append('<option selected="selected" value="">Please select one option</option>');
                        data = $.parseJSON(data);
                        _selectize[0].selectize.clear();
                        _selectize[0].selectize.clearOptions();
                        _selectize[0].selectize.addOption(data);
                    }
                });
            });


            var _selectize = $('select#questionnaire_subcategory').selectize({
                plugins: ['restore_on_backspace'],
                delimiter: ',',
                placeholder: 'Selectati intai tipul si categoria ',

                create: function (option) {
                    var id;
                    $.ajax({
                        async: false,
                        url: "/do_add_questionnaire_subcategory",
                        data: {subcategory_name: option, type_id: $('#questionnaire_type').val(), category_id :$('#questionnaire_category').val() },
                        type: 'POST',
                        success: function (data) {
                            id = jQuery.parseJSON(data);
                            return id;
                        }
                    });
                    $('#questionnaire_subcategory').append(
                            $('<option></option>').val(id).html(option)
                    );
                    return {
                        value: id,
                        text: option
                    }
                }
            });

            var _selectize_criteria = $('select.criteria').selectize({
                plugins: ['restore_on_backspace'],
                delimiter: ',',
                placeholder: 'Selectati criteriul de evaluare pentru intrebare ',

                create:false
            });

            $('#questionnaire_type,#questionnaire_category,#questionnaire_subcategory').change(function() {
                var type_id = $('#questionnaire_type').val();
                var category_id = $('#questionnaire_category').val();
                var subcategory_id = $('#questionnaire_subcategory').val();
                if(type_id != '' && category_id !='' && subcategory_id !='') {
                    $('.lista_intrebari').empty();
                    $.ajax({
                        url: "/get_questions/" + type_id + "/" + category_id + "/" + subcategory_id ,
                        type: 'GET',
                        success: function (data) {
                            data = $.parseJSON(data);

                            $.each(data, function(key, value){
                                $('#intrebare[' + key + ']').html(value.intrebare);
                                $('.lista_intrebari').append( "<div class = 'intrebare' id='intrebare[" + key + "]'>" +
                                        value.intrebare +
                                        "<div class = 'criteriu'>" + value.name  + "</div>" +
                                        "<div id ='raspuns_" + key + "'>" +
                                        "</div>" + "</div>"
                                );
                                $.each(value.answers, function(item, raspuns) {
                                    console.log(raspuns);
                                    $('#raspuns_' + key).append("<span  class ='raspuns' id = 'raspuns[" + key + "][" + item + "]'>"
                                    + raspuns.raspuns + " (" + raspuns.punctaj_raspuns + ") </span>");
                                });
                            });
                        }
                    });
                    get_evaluation_criteria(type_id, category_id, subcategory_id, _selectize_criteria);

                }

            });
        });

        function get_evaluation_criteria (type_id, category_id, subcategory_id, selectize)
        {
            $.ajax({
                url: "/get_evaluation_criteria_with_alias/" + type_id + "/" + category_id + "/" + subcategory_id ,
                type: 'GET',
                success: function (data) {
                    data = $.parseJSON(data);
                    selectize[0].selectize.clear();
                    selectize[0].selectize.clearOptions();
                    selectize[0].selectize.addOption(data);
                }
            });
        }

    </script>
@endsection
