@extends('layouts.app')

@section('content')
       <div class="container" ng-app="pacient_form">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Adauga intrebari la chestionar</div>
                    <div class="panel-body">

                        <form ng-submit="submit_question()" ng-controller="QuestionsAddCtrl">

                            <div id="question-group" class="form-group">
                                <label>Intrebare : </label><textarea ng-model="formData.intrebare" cols="80%"
                                                                     name="intrebare"></textarea>
                                <input class="btn btn-info btn-lg " type="button" id="add_options"
                                       ng-click="addFields()" value="Adauga variante raspuns"/>
                            </div>

                            <div class="form-group" ng-repeat="input in formData.answers track by $index">
                                <label>Raspuns : </label><textarea ng-model="input.raspuns" name="raspuns"
                                                                   cols="60%"></textarea>
                                <label>Punctaj : </label><input type="text" ng-model="input.punctaj_raspuns"
                                                                name="punctaj_raspuns"/>
                            </div>


                            <input class="btn btn-success btn-lg btn-block" type="submit" id="submit" value="Submit"/>
                        </form>
                        </div>
                    <div class="panel-heading">Lista intrebari</div>
                    <div class="panel-body">

                            <div ng-init="getList()" ng-controller="QuestionsAddCtrl">
                                <ul>
                                    <li ng-repeat="content in groups">
                                        <span editable-text="content.intrebare" >@{{content.intrebare}}</span>
                                        <td><span style='float: right;' class = "glyphicon glyphicon-trash text-danger"></span></td>
                                        <td>&nbsp;</td>
                                        <td><span style='float: right;' class = "glyphicon glyphicon-edit text-primary"></span></td>
                                        <ul>
                                            <li ng-repeat="answer in content.answers">


                                                <tr>
                                                    <td>@{{answer.raspuns }}</td>
                                                    <td>&nbsp;</td>
                                                    <td>@{{ answer.punctaj_raspuns }}</td>


                                                </tr>

                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
       <script src="/js/user_area/create_pacient.js"></script>
       <script src="/js/admin_area/QuestionsAddCtrl.js"></script>


@endsection
