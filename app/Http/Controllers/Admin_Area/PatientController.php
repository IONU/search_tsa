<?php
namespace App\Http\Controllers\Admin_Area;

use App\Services\Patients;

class PatientController extends AdminController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function list_patients()
    {
        $patients = Patients::get_users_pacients();
//        print_r($patients);

        return view('admin/list_pacients')->with('patients', $patients);

    }
}
