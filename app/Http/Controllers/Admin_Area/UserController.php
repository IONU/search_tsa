<?php
/**
 * Created by PhpStorm.
 * User: IONU
 * Date: 01-11-2015
 * Time: 21:24
 */

namespace App\Http\Controllers\Admin_Area;


use App\Models\User;

class UserController extends AdminController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function list_users()
    {
        $users = User::all();

        return view('admin/list_users')->with('users', $users);
    }
}
