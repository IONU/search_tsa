<?php
namespace App\Http\Controllers\Admin_Area;

use App\Models\QuestionnaireType;
use App\Services\Patients;
use App\Models\QuestionnaireCategory;
use App\Services\QuestionAnswerService;
use App\Services\QuestionnaireCategoryService;
use App\Services\QuestionnaireSubcategoryService;
use App\Services\QuestionnaireTypeService;
use App\Services\QuestionService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class QuestionnaireController extends AdminController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function do_add_questionnaire(Request $request)
    {
        $this->validate($request, [
            'questionnaire_type' => 'required',
            'questionnaire_category' => 'required',
            'questionnaire_subcategory' => 'required',
        ]);

        foreach ($request->question as $question) {
            $question['type_id'] = $request->questionnaire_type;;
            $question['category_id'] = $request->questionnaire_category;
            $question['subcategory_id'] = $request->questionnaire_subcategory;
            $this->do_add_questions($question);
        }
        return redirect('/admin/adauga_intrebari');
    }

    public function do_add_questionnaire_type()
    {
        $data = \Input::all();
        $quetionnaire_type = new QuestionnaireTypeService();
        return $quetionnaire_type->save_questionnaire_type($data);
    }

    public function do_add_questionnaire_category()
    {
        $data = \Input::all();
        $questionnaire_category = new QuestionnaireCategoryService();
        return $questionnaire_category->save_questionnaire_category($data);
    }

    public function do_add_questionnaire_subcategory()
    {
        $data = \Input::all();
        $questionnaire_category = new QuestionnaireSubcategoryService();
        return $questionnaire_category->save_questionnaire_subcategory($data);
    }

    public function add_questionnaire()
    {
        $questionnarie_types = QuestionnaireType::all();
        $questionnarie_categories = QuestionnaireCategory::all();
        return view ('admin.questionnaire.add_questionnaire')
            ->with('questionnarie_categories', $questionnarie_categories)
            ->with('questionnarie_types',$questionnarie_types);
    }

    public function get_subcategories()
    {
        $data = \Input::all();
        $subcategories = QuestionnaireSubcategoryService::get_all_subcategories_for_category($data['category_id'], $data['type_id']);
        return json_encode($subcategories);
    }

    public function get_questions($type_id, $category_id, $subcategory_id)
    {
        $question_answer = new QuestionAnswerService();
        $questions = QuestionService::get_questions_for_subcategoty($type_id, $category_id, $subcategory_id);
        foreach($questions as $question) {
            $question->intrebari = null;
            $question->answers = $question_answer->get_answers_for_question($question->id);
        }
        return json_encode($questions);
    }


}
