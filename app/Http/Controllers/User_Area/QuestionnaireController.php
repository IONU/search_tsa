<?php
namespace App\Http\Controllers\User_Area;

use App\Http\Controllers\BackEndController;
use App\Models\Question;
use Illuminate\Http\Request;
use App\Services;
use Validator;
use App\Models\Patient;
use App\Services\QuestionPatientService;
use App\Services\Users;
use App\Services\QuestionnaireTypeService;
use App\Services\Patients;
use App\Services\Counties;
use App\Services\QuestionnaireSubcategoryService;
use Illuminate\Support\Facades\DB;
use App\Services\QuestionService;



class QuestionnaireController extends BackEndController
{

    public function alege_chestionar(){
        $questionaire_types = QuestionnaireTypeService::get_rows_order_by([], 'questionnaire_type_name');
        $data['questionaires'] = $questionaire_types;
        return view('user_area.adauga_chestionar', $data);
    }

    public function aplica_chestionar($questionaire_id){
        
        $counties = Counties::get_rows_order_by([], 'name');
        
        $data_pacient['counties'] = $counties;
        $data['data_pacient'] = $data_pacient;
        DB::enableQueryLog();
        $questionnaire_subcategories = QuestionnaireSubcategoryService::get_categories_with_subcategories($questionaire_id);
         
        foreach ($questionnaire_subcategories as &$subcategory){
            $subcategory['questions'] = QuestionService::get_questions_for_subcategoty($questionaire_id, $subcategory['category_id'], $subcategory['id']);
        }
         
        $data['questionnaire_subcategories'] = $questionnaire_subcategories;
        $data['subcat_nr'] = count($questionnaire_subcategories)+2;
        
        return view('user_area.add_full_questionnaire', $data);
    }

    public function show_questionnaire($pacient_id)
    {
        return view('user_area.show_questionnaire')->with('pacient_id',$pacient_id);
    }
    public function view_questionnaire()
    {
        return view('user_area.view_questionnaire');
    }


    public function submit_questionnaire(Request $request)
    {
        $this->validate($request, [
            'nume' => 'required|string',
            'prenume' => 'required|string',
            'cnp' => 'required|numeric',
            'mama' => 'string',
            'tata' => 'string',
            'adresa' => 'required',
            'localitate' => 'required',
            'judet' => 'required',
            'email' => 'required|email',
            'telefon' => 'required',
        ]);

        $user = $request->user();
        $patient['nume'] = $request->nume;
        $patient['prenume'] = $request->prenume;
        $patient['cnp'] = $request->cnp;
        $patient['mama'] = $request->mama;
        $patient['tata'] = $request->tata;
        $patient['adresa'] = $request->adresa;
        $patient['judet'] = $request->judet;
        $patient['localitate'] = $request->localitate;
        $patient['email'] = $request->email;
        $patient['telefon'] = $request->telefon;
        $patient['observatii_medicale'] = $request->observatii_medicale;

        $users_service = new Users();
        $saved_patient=$users_service->save_pacient($user, $patient);

       echo json_encode($saved_patient);

//        $questionnaire_message= $this->process_questionnaire($saved_patient->id, $request);

//        return view('user_area.questionnaire_answer')->with('pacient_id',$saved_patient->id)->with('message', $questionnaire_message);

    }
    
    public function save_question_to_pacient($patient_id, Request $request){
        $data = [];
        $questions_patient = new QuestionPatientService();
        
        if(is_array($request->intrebari )){
            foreach ($request->intrebari as $intrebare => $raspuns) {
                $data['intrebare_id'] = $intrebare;
                $data['raspuns_id'] = $raspuns;
                $data['pacient_id'] = $patient_id;
        
                $questions_patient->save_question_patient($data);
            }
        }
    }

    public function process_questionnaire($patient_id, Request $request)
    {
        $questions_patient = new QuestionPatientService();

        $questions_list = Question::lists('intrebare', 'id');
        $answers = $request->all();

        if (!isset($answers['intrebari']) || count($answers['intrebari']) < count($questions_list)) {
            return \Redirect::back()->withErrors('Trebuie sa raspundeti la toate intrebarile ')->withInput($request->all());
        }

        $raspuns_important = $questions_patient->get_answers_for_patient($patient_id,1);
        $raspuns = $questions_patient->get_answers_for_patient($patient_id);
        $user = $request->user();
        $users_service = new Users();
        $patient = Patient::find($patient_id);
        $patient['punctaj'] =$raspuns['punctaj_total'];
        $patient['tested_at'] = date("Y-m-d H:i:s");

        if ($raspuns_important['punctaj_total'] >= 2 || $raspuns['punctaj_total'] >= 3) {
            $patient['posibil_autism'] = 1;
            $message = "In urma analizarii raspunsurilor introduse in chestionar exista suspiciuni legate de autism.
             Va recomandam sa continuati evaluarile la un specialist psiholog/psihiatru/pediatru sau sa apelati infoline Autism Baia Mare: 0800 500 288";
        } else {
            $message = "Nu sunt suspiciuni legate de autism.";
        }
        $users_service->save_pacient($user, $patient, $patient->id);
        return $message;
    }

}
