<?php

namespace App\Http\Controllers\User_Area;

use App\Http\Controllers\BackEndController;
use App\Models\User;
use App\Services\Users;
use Illuminate\Http\Request;

class UserController extends BackEndController{

    public function getDetalii(Request $request){
        $user = $request->user();
        return view('user_area/show_user')->with('user', $user);
    }

    public function getEdit(Request $request){
        $user = $request->user();
         return view('user_area/edit_user')->with('user', $user);
    }

    public function postSave(Request $request){

        $data['name'] = $request->name ;
        $data['email'] = $request->email ;
        $data['user_type']= $request->user_type ;

        $user = new Users();
        $saved_user=$user->save_user($data,$request->user()->id );

        return view('user_area/show_user')->with('user', $saved_user);

    }

    public function getChangepassword(){

        return view('user_area/change_password');
    }

    public function postPassword()
    {
        $user = \Auth::user();
        $rules = array(
            'password' => 'required',
            'new_password' => 'required|confirmed|min:6',
            'new_password_confirmation'=> 'required'
        );

        $data = \Input::all();

        $validator = \Validator::make($data, $rules);

        if ($validator->fails())
        {
            return \Redirect::back()->withErrors($validator);
        }
        else
        {
            if (!\Hash::check(\Input::get('password'), $user->password))
            {
                return \Redirect::back()->withErrors('Parola actuală este greșită');
            }
            else
            {
                $user->password = \Hash::make(\Input::get('new_password'));
                $user->save();
                return \Redirect::to('user/detalii');
            }
        }
    }


}