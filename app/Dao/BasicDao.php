<?php
/*
 * basci database access
 */
namespace App\Dao;

abstract class BasicDao
{
	
	static function get_rows_order_by($where, $order_by){
		$model_name = static::$model;
		$query = $model_name::select("*");
		foreach ($where as $key=>$value){
			$query->where($key,$value);
		}
		return $query->orderBy($order_by)->get();
	}
	
	static function get_rows($where){
		$model_name = static::$model;
		$query = $model_name::select("*");
		foreach ($where as $key=>$value){
			$query->where($key,$value);
		}
		
		return $query->get();
	}
}