<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuestionnaireType extends Model{

    protected $table = 'questionnaire_type';

    public $timestamps = false;
}
