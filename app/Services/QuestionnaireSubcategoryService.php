<?php
namespace App\Services;

use \App\Models\QuestionnaireSubcategory;
use Illuminate\Support\Facades\DB;
use App\Dao\BasicDao;
Class QuestionnaireSubcategoryService extends BasicDao
{
    static $model = '\App\Models\QuestionnaireSubcategory';

    public function save_questionnaire_subcategory($data, $questionnaire_id = null)
    {

        if ($questionnaire_id == null) {
            $questionnaire = new QuestionnaireSubcategory();
        } else {
            $questionnaire = QuestionnaireSubcategory::find($questionnaire_id);
        }

        foreach ($data as $key => $save_data) {
            $questionnaire->$key = $data[$key];
        }
        $questionnaire->save();

        return $questionnaire->id;

    }

    public static function get_all_subcategories_for_category($categary_id, $type_id)
    {
        return QuestionnaireSubcategory::where('category_id', $categary_id)
            ->select(DB::raw('subcategory_name AS text, id AS value'))
            ->where('type_id', $type_id)
            ->orderBy('id','ASC')
            ->get();
    }

    public static function get_categories_with_subcategories($type_id){
        return QuestionnaireSubcategory::
        select(DB::raw("questionnaire_subcategory.category_id, questionnaire_subcategory.id, category_name, questionnaire_subcategory.subcategory_name"))
        ->join("questionnaire_category AS cat", "category_id", "=", "cat.id")
        ->where('questionnaire_subcategory.type_id',$type_id)->get();
        
    }
    
}