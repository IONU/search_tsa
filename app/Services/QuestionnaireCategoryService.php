<?php namespace App\Services;

use \App\Models\QuestionnaireCategory;
use App\Dao\BasicDao;
Class QuestionnaireCategoryService extends BasicDao
{

    static $model = '\App\Models\QuestionnaireCategory';

    public function save_questionnaire_category($data, $questionnaire_id = null)
    {

        if ($questionnaire_id == null) {
            $questionnaire = new QuestionnaireCategory();
        } else {
            $questionnaire = QuestionnaireCategory::find($questionnaire_id);
        }

        foreach ($data as $key => $save_data) {
            $questionnaire->$key = $data[$key];
        }
        $questionnaire->save();

        return $questionnaire->id;

    }

    

}