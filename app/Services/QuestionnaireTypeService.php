<?php
namespace App\Services;

use \App\Models\QuestionnaireType;
use App\Dao\BasicDao;
Class QuestionnaireTypeService extends BasicDao
{
    static $model = '\App\Models\QuestionnaireType';

    public function save_questionnaire_type($data, $questionnaire_id = null)
    {

        if ($questionnaire_id == null) {
            $questionnaire = new QuestionnaireType();
        } else {
            $questionnaire = QuestionnaireType::find($questionnaire_id);
        }

        foreach ($data as $key => $save_data) {
            $questionnaire->$key = $data[$key];
        }
        $questionnaire->save();

        return $questionnaire->id;

    }


}