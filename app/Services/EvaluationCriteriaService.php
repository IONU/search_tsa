<?php
namespace App\Services;

use App\Dao\BasicDao;
use \App\Models\EvaluationCriteria;
use Illuminate\Support\Facades\DB;
Class EvaluationCriteriaService extends BasicDao
{

    static $model = '\App\Models\EvaluationCriteria';

    public function save_evaluation_criteria($data, $criteria_id = null)
    {

        if ($criteria_id == null) {
            $criteria = new EvaluationCriteria();
        } else {
            $criteria = EvaluationCriteria::find($criteria_id);
        }

        foreach ($data as $key => $save_data) {
            $criteria->$key = $data[$key];
        }
        $criteria->save();

        return $criteria->id;

    }

    public static function get_all_evaluation_criteria_for_subcategory( $type_id, $categary_id, $subcategory_id)
    {
        return EvaluationCriteria::where('category_id', $categary_id)
            ->where('subcategory_id', $subcategory_id)
            ->where('type_id', $type_id)
            ->select(DB::raw('name AS text, id AS value'))
            ->orderBy('id','ASC')
            ->get();
    }


}